﻿using UnityEngine;
using System.Collections;

public class gunChewt : MonoBehaviour
{
    //RaycastHit hit;
    //public float mouseX;
    //public float mouseY;
    //public float mouseZ;
    //public float relmousepos1;
    //public float zDistance = -10f;
    float shotClock;
    public float attackSpeed = .5f;
    public GameObject bulletPrefab;
    public GameObject bulletPosition;
    public float bulletSpeed = 5;
    bool isMirrored;
    public float kickBackAmount = 1;
    //public float changeAngleX;
    //public float changeAngleY;
    //public float changeAngleZ;
    //public float changeAngelW;
    //GameObject lastFiredRound;
    //public float bulletSpeed = 1;
    GameObject playa;

    // Use this for initialization
    void Start()
    {
        playa = GameObject.Find("thePlayer");
    
    }
    
    // Update is called once per frame
    void Update()
    {
        //changeAngleX = transform.rotation.x;
        //changeAngleY = transform.rotation.y;
        //changeAngleZ = transform.rotation.z;
        //changeAngelW = transform.rotation.w;
        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        Vector2 mousePos = mouseRay.GetPoint(10);

        //mouseX = transform.rotation.x;
        //mouseY = transform.rotation.y;
        //mouseZ = transform.rotation.z;
        //Vector3 mousePos = Camera..ScreenToWorldPoint (Input.mousePosition);//Camera.main.ScreenToWorldPoint (Input.mousePosition);
        //mouseX = mousePos.x;
        //mouseY = mousePos.y;
        //mouseZ = mousePos.z;
        Vector2 relMousePos = mousePos - new Vector2(transform.position.x, transform.position.y);
        //relmousepos1 = relMousePos;
        Vector2 relMousePosNorm = relMousePos.normalized; //if mirrored, relMousePosNorm.x *= -1
        //mouseX = relMousePosNorm.x
        //mouseY = relMousePosNorm.y;
        if (relMousePos.x < 0)
        {
            relMousePosNorm.x *= -1;
            isMirrored = true;
        } else
        {
            isMirrored = false;
        }
        Quaternion rotationToMouse = Quaternion.FromToRotation(Vector3.right, relMousePosNorm);
        transform.rotation = rotationToMouse;






        //rotation to face mouse - to be resolved later

        //transform.rotation = Quaternion.LookRotation (Vector3.forward, mousePos - transform.position);
        //Input.mouse
        /*Vector3 mousePos = Camera.current.ScreenToWorldPoint (new Vector3(Input.mousePosition.x, Input.mousePosition.y, zDistance));
        mouseX = mousePos.x;
        mouseY = mousePos.y;
        mouseZ = mousePos.z;
        transform.rotation = Quaternion.Euler (0, 0, Mathf.Atan2 (mousePos.y, mousePos.x) * Mathf.Rad2Deg - 90);
        //Vector2 mousePos = new Vector2(Camera.main.ScreenPointToRay(Input.mousePosition).);
        //transform.rotation = Quaternion.Euler (0, 0, Mathf.Atan2(mousePos.x - transform.position.x, mousePos.y - transform.position.y) * Mathf.Rad2Deg - 90);
        // - transform.position.y, - transform.position.x
        */
        if (shotClock < attackSpeed)
        {
            shotClock += Time.deltaTime;
        }
        //the shooting part of the gun

    }

    public void Attack(Movement tracker)
    {
        if (shotClock >= attackSpeed)
        {
            Vector3 newMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            newMousePos.z = 0;
            //if(transform.localScale
            Vector3 directionToFire = newMousePos - transform.position;
            directionToFire.Normalize();
            Quaternion angleToFire = Quaternion.FromToRotation(Vector3.right, directionToFire);
            GameObject lastFiredBullet = (GameObject)GameObject.Instantiate(bulletPrefab, bulletPosition.transform.position, angleToFire);
            lastFiredBullet.rigidbody2D.velocity = directionToFire * bulletSpeed;
            this.audio.Play();


            playa.rigidbody2D.AddForce(-1*directionToFire * kickBackAmount,ForceMode2D.Impulse);



            //lastFiredBullet.transform.localRotation = 


            //lastFiredBullet.rigidbody2D.velocity = new Vector2(
            //transform.position
            //mouseX
            //mouseY









            //rigidbody2D.velo
            //lastFiredBullet.transform.rotation = new Quaternion(0,0,0,0);
            if (isMirrored)
            {
                //lastFiredBullet.transform.rotation = -1* transform.rotation;
                //transform.
                //lastFiredBullet.transform.rotation = new Quaternion(transform.rotation.x + changeAngleX, transform.rotation.y + changeAngleY, transform.rotation.z + changeAngleZ, transform.rotation.w + changeAngelW);
            }
            //lastFiredBullet.SendMessage("MirrorBullet",SendMessageOptions.DontRequireReceiver);

            //lastFiredBullet.rigidbody2D.AddRelativeForce (Vector2.right * bulletSpeed, ForceMode2D.Impulse);
            shotClock = 0;
            tracker.ammoCount -= 1;
        }
    }

    public void MirrorGun(bool newMirror)
    {
        isMirrored = newMirror;
    }
}
    