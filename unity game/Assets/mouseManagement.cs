﻿using UnityEngine;
using System.Collections;

public class mouseManagement : MonoBehaviour {
	public bool inRTSMode;
	RaycastHit rayHit;
	ArrayList selectedArmy = new ArrayList(10);
	ArrayList totalArmy = new ArrayList();
	ArrayList enemyArmy = new ArrayList ();
	bool attackMoveOn;
	public int carrySize = 8;
	public int carrySize2 = 9;
	public float mouseSpotx;
	public float mouseSpoty;
	public float mouseSpotz;
	public bool targetFound;
    //public float clickCooldown = .25f;
    //float clickClock;
	//System.Collections.


	// Use this for initialization
	void Start () {
		foreach (GameObject smonster in GameObject.FindGameObjectsWithTag("Enemy")) {
						enemyArmy.Add (smonster);
				}
		foreach (GameObject sminion in GameObject.FindGameObjectsWithTag("Minion")) {
						totalArmy.Add (sminion);
				}
		//gameObject.tag = "Selected";
	
	}
	
	// Update is called once per frame
	void Update () {
        //if (clickClock < clickCooldown)
        //{
            //clickClock += Time.deltaTime;
        //}
		//mouseSpotz = Input.mousePosition.z;
        //else
        //{

		Ray testRay = Camera.main.ScreenPointToRay (Input.mousePosition);
		//Debug.DrawRay (testRay.origin, testRay.direction, Color.yellow,20f);
				carrySize = totalArmy.Count;
				carrySize2 = selectedArmy.Count;
				if (inRTSMode) {
						if (Input.GetButtonDown ("MouseClick")) {//when the player left clicks, stuff is selected
								//Ray mouseRay = Camera.main.ScreenPointToRay (Input.mousePosition);//creates the mouse point
				Vector3 newMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    //clickClock = 0;
				newMousePos.z=0;

				//print (Input.mousePosition.ToString() + " " + newMousePos.ToString());
								//Vector2 mousePos = mouseRay.GetPoint(5f); //is the mouse point
				//Vector3 newMousePos = new Vector3(mouseRay.origin.x,mouseRay.origin.y,0);
								//mouseSpotx = mousePos.x;
								//mouseSpoty = mousePos.y;
				mouseSpotx = newMousePos.x;
				mouseSpoty = newMousePos.y;
								bool unitSelected = false;
								foreach (GameObject testClicked in totalArmy) {
										//if ((Mathf.Abs (testClicked.transform.position.x - mousePos.x) <= .75f) && (Mathf.Abs (testClicked.transform.position.y - mousePos.y) <= .75f)) {
					if ((Mathf.Abs (testClicked.transform.position.x - newMousePos.x) <= 1.25f) && (Mathf.Abs (testClicked.transform.position.y - newMousePos.y) <= 1.25f)) {

												if (!selectedArmy.Contains (testClicked)) {
														selectedArmy.Add (testClicked);
														unitSelected = true;
												}
										}
								}
								if (unitSelected == false) {
										selectedArmy.Clear ();
								}
						}
						if (Input.GetButtonDown ("MouseClick1")) { //when player right-clicks, units are ordered around
                    //clickClock = 0;
				//Ray mouseRay = Camera.main.ScreenPointToRay (Input.mousePosition);//creates the mouse point
				Vector3 newMousePoint = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));

				//Vector2 mousePos = mouseRay.GetPoint(5f); //is the mouse point
				//Vector3 newMousePoint = new Vector3(mouseRay.origin.x,mouseRay.origin.y,0);

				//mouseSpotx = mousePos.x;
				//mouseSpoty = mousePos.y;
				mouseSpotx = newMousePoint.x;
				mouseSpotx = newMousePoint.y;
								if (!attackMoveOn) {
										targetFound = false;
										foreach (GameObject testTarget in enemyArmy) {
												//if ((Mathf.Abs (testTarget.transform.position.x - mousePos.x) <= .5f) && (Mathf.Abs (testTarget.transform.position.y - mousePos.y) <= .5f)) {
						if ((Mathf.Abs (testTarget.transform.position.x - newMousePoint.x) <= .025f) && (Mathf.Abs (testTarget.transform.position.y - newMousePoint.y) <= .025f)) {
														targetFound = true;
														foreach (GameObject unit in selectedArmy) {
																unit.SendMessage ("AcquireTarget", testTarget, SendMessageOptions.DontRequireReceiver);
														}
												}
										}
										if (targetFound == false) {
												foreach (GameObject unit in selectedArmy) {
														//unit.SendMessage ("AcquirePoint", mousePos, SendMessageOptions.DontRequireReceiver);
							unit.SendMessage ("AcquirePoint", newMousePoint, SendMessageOptions.DontRequireReceiver);
												}
										}
								}
								else
				{
					foreach(GameObject unit in selectedArmy)
					{
						unit.SendMessage ("AttackMoveOrder", newMousePoint, SendMessageOptions.DontRequireReceiver);
					}
				}
						}
				}
		//}
}
}


						//Ray mouseRay = Camera.main.ScreenPointToRay (Input.mousePosition); //check where mouse pointer is
			//Debug.DrawRay(mouseRay.origin, mouseRay.direction * 20, Color.yellow);
						
						/*if (Input.GetButton ("MouseClick")) { //when player clicks, units are added or deselected
								if (Physics.Raycast (mouseRay, out rayHit, 10)) { //if the ray hit something
					objectTag = rayHit.collider.gameObject.name;
										if (rayHit.collider.gameObject.tag == "Minion") {
												if (selectedArmy.Count < selectedArmy.Capacity) {
														rayHit.collider.gameObject.SendMessage ("ToggleDisplay", true, SendMessageOptions.DontRequireReceiver);
														selectedArmy.Add (rayHit.collider.gameObject);
														rayHit.collider.gameObject.tag = "Selected";
												}
										} else {
												selectedArmy.Clear ();
										}
								}
						}
						if(Input.GetButton ("MouseClick1")) { //when player right-clicks, units are ordered around
								if (!attackMoveOn) {
										if (Physics.Raycast (mouseRay, out rayHit, 10)) { //if ray hit point,

												if (rayHit.collider.gameObject.tag == "Enemy") {
							objectTag = rayHit.collider.gameObject.name;
														foreach (GameObject soldier in selectedArmy) {
																soldier.SendMessage ("AcquireTarget", rayHit.collider.gameObject, SendMessageOptions.DontRequireReceiver);
														}
												} else {
														foreach (GameObject soldier in selectedArmy) {
																soldier.SendMessage ("AcquirePoint", mouseRay.GetPoint (30), SendMessageOptions.DontRequireReceiver);

														}
												}
										}
								} else {
										foreach (GameObject soldier in selectedArmy) {
												soldier.SendMessage ("AttackMoveOrder", mouseRay.GetPoint (30), SendMessageOptions.DontRequireReceiver);
										}
								}
						}*/
	/*void TestMouse()
	{
				Ray mouseRay = Camera.main.ScreenPointToRay (Input.mousePosition);
				Vector2 mousePos = mouseRay.GetPoint (10);
				Vector2 relMousePos = mousePos - new Vector2 (transform.position.x, transform.position.y);
				Vector2 relMousePosNorm = relMousePos.normalized;
		}*/
