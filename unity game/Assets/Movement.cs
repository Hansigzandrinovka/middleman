﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour
{
	public bool isControllable = true;
	public int health = 5;
	public float invincibleTime = .5f;
	float invincibleClock;
		public float moveSpeed;
		public float moveFactor = 1;
	public float moveMaximum = 6;
	GameObject currentWeapon;
	int currentWeaponIndex;
	int potionCount;
	public int ammoCount;
	public GameObject swordSlot;
	public GameObject gunSlot;
	Animator theAnimator;
	public float verVelo;
	public float jumpTime = .25f;
	public float jumpClock;
	public AudioClip hurtSound;
	public AudioClip runSound;
	float walkSoundTime = .25f;
	float walkSoundClock;

		// Use this for initialization
		void Start ()
		{
		//GameObject.Find ("Main Camera").camera.transform.Translate(new Vector3(0,.5f,0));
		//GameObject.
			jumpClock = jumpTime;
		theAnimator = GetComponent<Animator> ();

		}
	
		// Update is called once per frame
			void Update ()
		{
        theAnimator.SetBool("IsFrozen", !isControllable);
        if (isControllable)
        { //the player only interracts with the rest of the world if he is controllable (not in RTS mode)

            if (invincibleClock < invincibleTime)
            {
                invincibleClock += Time.deltaTime;
            }
            if (jumpClock < jumpTime)
            {
                jumpClock += Time.deltaTime;
                if (Input.GetButton("Jump"))
                {
                    this.transform.rigidbody2D.AddForce(Vector2.up * .25f, ForceMode2D.Impulse);
                }
            }

            moveSpeed = Input.GetAxis("Movement");
            //Mathf.abs
            if ((Mathf.Abs(moveSpeed * moveFactor) > 0) && (Mathf.Abs(moveSpeed * moveFactor) <= moveMaximum))
            {

                transform.Translate(new Vector3(moveSpeed * moveFactor, 0, 0) * Time.deltaTime);
                theAnimator.SetBool("IsMoving", true);
                if (Mathf.Abs(rigidbody2D.velocity.y) == 0)
                {
                    TryPlayWalkSound();
                }

                //make player facing change when moving
                if ((moveSpeed > 0) && (currentWeaponIndex != 2))
                {
                    transform.localScale = new Vector3(1, 1, 1);
                } else if ((moveSpeed < 0) && (currentWeaponIndex != 2))
                {
                    transform.localScale = new Vector3(-1, 1, 1);
                }
            } else if (moveSpeed == 0)
            {
                theAnimator.SetBool("IsMoving", false);
            }
            //rigidbody2D.AddForce ((moveSpeed * moveFactor), 0f, 0f, ForceMode.Force); 
            if (Input.GetButton("MouseClick"))
            {
                if (!((currentWeaponIndex == 2) && (ammoCount == 0)))
                { // only false if you have machine gun out, and no ammo
                    if (currentWeapon != null)
                    {
                        if (currentWeaponIndex == 1)
                        { //sword
                            currentWeapon.SendMessage("Attack", SendMessageOptions.DontRequireReceiver);
                            if (transform.localScale.x == -1)
                            {
                                currentWeapon.SendMessage("MirrorGun", true, SendMessageOptions.DontRequireReceiver);
                            } else
                            {
                                currentWeapon.SendMessage("MirrorGun", false, SendMessageOptions.DontRequireReceiver);
                            }
                        } else if (currentWeaponIndex == 2)
                        { //gun
                            currentWeapon.SendMessage("Attack", this, SendMessageOptions.DontRequireReceiver);
                        }
                    }
                }
            }
            if ((Input.GetButton("EquipSword")) && (currentWeaponIndex != 1))
            {
                if (currentWeapon != null)
                {
                    GameObject.Destroy(currentWeapon);
                }
                float orientation = transform.localScale.x;
                currentWeapon = GameObject.Instantiate(swordSlot, new Vector3(transform.position.x + .5f, transform.position.y + .75f, transform.position.z), transform.rotation) as GameObject;
                currentWeapon.transform.parent = transform;
                currentWeapon.transform.localScale = new Vector3(1, 1, 1);
                currentWeaponIndex = 1;
            }
            if ((Input.GetButton("EquipGun")) && (currentWeaponIndex != 2))
            {
                if (currentWeapon != null)
                {
                    GameObject.Destroy(currentWeapon);
                }

                if (transform.localScale.x == -1)
                {
                    currentWeapon = GameObject.Instantiate(gunSlot, new Vector3(transform.position.x - .5f, transform.position.y, transform.position.z), transform.rotation) as GameObject;
                    currentWeapon.transform.localScale = new Vector3(-1, 1, 1);
                } else
                {
                    currentWeapon = GameObject.Instantiate(gunSlot, new Vector3(transform.position.x + .5f, transform.position.y, transform.position.z), transform.rotation) as GameObject;
                }
                /*if(transform.localScale.x == -1)
				{
					currentWeapon = GameObject.Instantiate (gunSlot, new Vector3 (transform.position.x - .5f, transform.position.y, transform.position.z), transform.rotation) as GameObject;
				}
				else{
					currentWeapon = GameObject.Instantiate (gunSlot, new Vector3 (transform.position.x + .5f, transform.position.y, transform.position.z), transform.rotation) as GameObject;
				}*/
                currentWeapon.transform.parent = transform;
                currentWeaponIndex = 2;
            }
            verVelo = rigidbody2D.velocity.y;
            if ((Input.GetButton("Jump")) && (this.transform.rigidbody2D.velocity.y == 0) && (jumpClock >= jumpTime))
            {

                this.transform.rigidbody2D.AddForce(Vector2.up * 8, ForceMode2D.Impulse);
                this.audio.Play();
                jumpClock = 0;
            }


            //rotates based on mouse
            if ((currentWeaponIndex == 2))
            { //rotate by pi
                Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (mouseRay.GetPoint(20).x > transform.position.x)
                {
                    transform.localScale = new Vector3(1, 1, 1);
                    //currentWeapon.transform.localScale = new Vector3 (1, 1, 1);
                } else
                {
                    transform.localScale = new Vector3(-1, 1, 1);
                    //currentWeapon.transform.localScale = new Vector3(-1,1,1);
                }
                //gunSlot = null;
            }

        } else
        {
            if(currentWeapon != null)
            {
                GameObject.Destroy(currentWeapon);
            }
            currentWeaponIndex = 0;
        }
    }
	void TakeDamage(int amount)
	{
				if (invincibleClock >= invincibleTime) {
						theAnimator.SetTrigger ("IsHurt");
			AudioSource.PlayClipAtPoint(hurtSound,transform.position);
						health -= amount;
						if (health <= 0) {
								//game over
						}
				}
		}
	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Enemy") {
						if (other.gameObject.transform.position.x > this.transform.position.x) {
								this.gameObject.rigidbody2D.AddForce (new Vector2 (-4, 3), ForceMode2D.Impulse);
						} else if (other.gameObject.transform.position.x < this.transform.position.x) {
								this.gameObject.rigidbody2D.AddForce (new Vector2 (4, 3), ForceMode2D.Impulse);
						}
				}
		}
	void TryPlayWalkSound()
	{
				if (walkSoundTime <= walkSoundClock) {
						AudioSource.PlayClipAtPoint (runSound,transform.position);
						walkSoundClock = 0;
						//float walkSoundTime = 1;
						//float walkSoundClock;
						//public AudioClip runSound;
				} else {
						walkSoundClock += Time.deltaTime;
				}
		}
}
