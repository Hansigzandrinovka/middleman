﻿using UnityEngine;
using System.Collections;

public class applyDamage : MonoBehaviour {
	public float duration = .3f;
	public int swordDamage = 10;
	float durclock;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
				if (durclock < duration) {
						durclock += Time.deltaTime;
				} else {
						GameObject.Destroy (this.gameObject);
				}
	
	}
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Enemy") {
			other.gameObject.SendMessage ("TakeDamage", swordDamage, SendMessageOptions.DontRequireReceiver);
			//GameObject.Destroy (this.gameObject);
		}
	}
}
