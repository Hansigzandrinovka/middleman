﻿using UnityEngine;
using System.Collections;

public class tankController : MonoBehaviour {
	//public bool rtsMode; //handled by player
	public int health = 5;
	public int damage = 1;
	public float speed = 1;
	public GameObject projectile;
	public Vector3 movePosition; //only exists when there is no target
	public GameObject target; //only exists when there is no point to go to
	bool attackMoving;
	public Transform firingPoint;
	public float attackSpeed = 1;
	float attackClock;
	//mouseManagement theMaster;
	public bool moving;
	//public int movePosX;
	//bool hasMoveOrder;
	//RaycastHit hit;

	//attackmove option for later

	// Use this for initialization
	void Start () {
		//theMaster = FindObjectOfType<mouseManagement> ();
	
	}
	
	// Update is called once per frame
	void Update () {

		if (attackClock <= attackSpeed) {
						attackClock += Time.deltaTime;
				}
		if ((!attackMoving) && (moving)) { //will not engage targets while moving
						GoToPoint (movePosition);
				} else if (attackMoving && (target == null)) { //attack move causes motion when no target
						GoToPoint (movePosition);
				} else if (target != null) { //if no motion, and there is a target, attack it
						AttackTarget ();
				}
		/*if ((Input.GetButton ("MouseClick")) && active) {
			Ray mouseRay = Camera.main.ScreenPointToRay (Input.mousePosition);
				}*/
	
	}
	void GoToPoint(Vector3 targetPoint)
	{
						if (targetPoint.x > (transform.position.x + .1f)) {
								transform.Translate (new Vector3 (speed * Time.deltaTime, 0, 0));
						} else if (targetPoint.x < (transform.position.x - .1f)) {
								transform.Translate (new Vector3 (speed * -1f * Time.deltaTime, 0, 0));
						}
		}
	void AttackTarget()
	{
		if (attackClock >= attackSpeed) {
						Vector3 relTarget = target.transform.position - transform.position;
						Vector3 relTargetNorm = relTarget.normalized;
						Quaternion rotationToTarget = Quaternion.FromToRotation (Vector3.right, relTargetNorm);
						GameObject firedround = GameObject.Instantiate (projectile, firingPoint.position, rotationToTarget) as GameObject;
				}
		//transform.rotation = rotationToTarget;
				
		//firedround.transform.LookAt(target.transform.position, (transform.position + new Vector3(0,0,1)));
		


		}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            target = other.gameObject;
        }
    }
	void AcquireTarget(GameObject newTarget)
	{
		moving = false;
				target = newTarget;
		attackMoving = false;
		}
	void AcquirePoint(Vector3 newMovePosition)
	{
		target = null;
		movePosition = newMovePosition;
		moving = true;
		attackMoving = false;

				}
	void AttackMoveOrder(Vector3 newMovePosition)
	{
		movePosition = newMovePosition;
		attackMoving = true;
		}
}
