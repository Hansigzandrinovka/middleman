﻿using UnityEngine;
using System.Collections;

public class EnemyWalk_Basic : MonoBehaviour {
	public int health = 20;
	public int attack = 1;
	bool direction; //false is left, true is right
	public float Speed = 2;
	//public float invincibleTime = .5f;
	//public float invincibleClock;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		/*if (invincibleClock < invincibleTime) {
						invincibleClock += Time.deltaTime;
				}*/
		if (direction) {
						transform.Translate (new Vector3 (Speed, 0, 0) * Time.deltaTime);
				} else {
						transform.Translate (new Vector3 (-1 * Speed, 0, 0) * Time.deltaTime);
				}
	
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Wall")
			{
			direction = !direction;
			}
		}
	void OnCollisionEnter2D(Collision2D other)
	{
				if ((other.gameObject.transform.position.x > this.transform.position.x) && (other.gameObject.tag == "Enemy")) {
						direction = !direction;
			rigidbody2D.AddForce(Vector2.right * -2f, ForceMode2D.Impulse);
				}
				if ((other.gameObject.tag == "Player") || (other.gameObject.tag == "Minion")) {
						other.gameObject.SendMessage ("TakeDamage", attack, SendMessageOptions.DontRequireReceiver);
				}
		}
	void TakeDamage(int amount)
	{
		//if (invincibleClock >= invincibleTime) {
			//invincibleClock = 0f;
						health -= amount;
						if (health <= 0) {
								GameObject.Destroy (this.gameObject);
						}
				//}
		}
}
