﻿using UnityEngine;
using System.Collections;

public class GUIManagement : MonoBehaviour {
	public bool playMode = true; //true is action, false is RTS
	public Texture rtsButtonTexture;
	public Texture actnButtonTexture;
	public mouseManagement mouseManager;
	public Movement PlayerControllinator;
	GameObject player;
	//GameObject AltCharacter;
	//GUI

	// Use this for initialization
	void Start () {
		player = GameObject.Find ("thePlayer");
		//PlayerCharacter = GameObject.Find ("thePlayer");
		//AltCharacter = GameObject.Find ("zonedOutPlayer");
	
	}
	
	// Update is called once per frame
	void OnGUI () {
		//GUI.DrawTexture (new Rect (0, 0, 128, 256), actnButtonTexture, ScaleMode.ScaleToFit);
		if (playMode) {
			GUI.DrawTexture (new Rect (0, 0, 256, 128), rtsButtonTexture);
			//GUI.DrawTexture(
				} else {
			GUI.DrawTexture (new Rect (0, 0, 256, 128), actnButtonTexture);
				}
		if (GUI.Button (new Rect (0, 0, 256, 128), "", new GUIStyle ())) {//the change-playmode button
						playMode = !playMode;
			SwapPlayerMode();
				}
		//if (mouseManager.inRTSMode == playMode) {
						//mouseManager.inRTSMode = !playMode;
		//		}
	
	}
	void SwapPlayerMode() //switches the player between "rts mode" and "action mode"
	{
		PlayerControllinator.isControllable = !PlayerControllinator.isControllable;
        if (playMode)
        {
            player.layer = 9;
        } else
        {
            player.layer = 12;
        }
		mouseManager.inRTSMode = !mouseManager.inRTSMode;
		audio.Play ();
	}
}
